#include <QMessageBox>
#include "network.h"
#include <QtGui>
#include <QSignalMapper>
#include <iostream>
#include "remotescreen.h"
#include <QString>

#define REMOTE_CMD_OK 0x00
#define REMOTE_CMD_SCREEN_SIZE 0x01
#define REMOTE_CMD_SEND_LINE 0x02
#define REMOTE_CMD_UPDATE 0x03

#define REMOTE_CMD_INVALID 0xFF

namespace {
    int read_int_from_pos(const QByteArray &array, size_t pos)
    {
        return (((array.at(pos + 3) & 0xFF) << 24) +
                ((array.at(pos + 2) & 0xFF) << 16) +
                ((array.at(pos + 1) & 0xFF) << 8) +
                ((array.at(pos + 0) & 0xFF)));
    }
}

server::server(QObject *parent, RemoteScreen *pScreen) :
    QTcpServer(parent), 
    m_pScreen(pScreen)
{
    connect( &m_ServerSocket, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(tcpError(QAbstractSocket::SocketError)) );
    connect( &m_ServerSocket, SIGNAL(readyRead()),
             this, SLOT(tcpReady()) );
    m_ServerSocket.setSocketOption(QAbstractSocket::KeepAliveOption, true );
    m_pSignalMapper = new QSignalMapper(parent);
}

server::~server() 
{
    m_ServerSocket.disconnectFromHost();
    m_ServerSocket.waitForDisconnected();
}

void server::tcpReady() 
{
    QByteArray array = m_ServerSocket.read(m_ServerSocket.bytesAvailable());
    QMessageBox::information( (QWidget *)this->parent(), tr("Information"),tr("Got Data"));
}

void server::tcpError(QAbstractSocket::SocketError error) 
{
    QMessageBox::warning( (QWidget *)this->parent(), tr("Error"),tr("TCP error: %1").arg( m_ServerSocket.errorString() ) );
}

bool server::start_listen(quint16 port_no) 
{
    if( !this->listen( QHostAddress::Any, port_no ) ) {
        QMessageBox::warning( (QWidget *)this->parent(), tr("Error!"), tr("Cannot listen to port %1").arg(port_no) );
        return false;
    }
    connect(this, SIGNAL(newConnection()), this, SLOT(initConnect()));
    return true;
}

void server::initConnect()
{
    std::cout << "initConnect\n";
    while (this->hasPendingConnections ()) {
        m_pClient = this->nextPendingConnection();
        connect(m_pClient, SIGNAL(disconnected()), m_pClient, SLOT(deleteLater()));
        connect(m_pClient, SIGNAL(readyRead()), m_pSignalMapper, SLOT(map()));
        m_pSignalMapper->setMapping(m_pClient, m_pClient);
    }
    connect(m_pSignalMapper, SIGNAL(mapped(QObject *)), this, SLOT(clientReadyRead(QObject *)));    
}

bool server::send_ok(QTcpSocket *pClientSocket)
{
    qDebug() << "Send OK\n";
    char data[256];
    memset(data,0,256);
    pClientSocket->write(data,1);
}

bool server::processData(const QByteArray &buffer, QTcpSocket *pClientSocket)
{
    char cmd;
    if (buffer.size() > 0)
    {
        cmd = buffer.at(0);
        qDebug() << "CMD: " << static_cast<quint8>(buffer[0]);
        switch (cmd)
        {
            case REMOTE_CMD_SCREEN_SIZE:
                {
                    int x = read_int_from_pos(buffer, 1);
                    int y = read_int_from_pos(buffer, 5);
                    qDebug() << "X: "<< x << "Y: " << y << "\n";
                    send_ok(pClientSocket);
                }
                return true;
            case REMOTE_CMD_SEND_LINE:
                {
                    int num = read_int_from_pos(buffer, 1);
                    int offset = read_int_from_pos(buffer, 5);
                    int length = read_int_from_pos(buffer, 9);
                    qDebug() << "NUM: " << num << " OFFSET: " << offset << " LENGTH: " <<length << "\n";
                    QByteArray data;
                    data = buffer.mid(13, length);
                    m_pScreen->draw_line(num, offset, data);
                }
                send_ok(pClientSocket);
                return true;
            case REMOTE_CMD_UPDATE:
                qDebug() << "Update\n";
                m_pScreen->update();
                send_ok(pClientSocket);
                return true;
        }
    }
    return false;
}

void server::clientReadyRead(QObject *socket)
{
    std::cout << "clientReadyRead\n";
    QByteArray buffer = m_pClient->readAll();
    qDebug() << m_pClient->bytesAvailable();
    if( buffer.size() > 0)
    {
        qDebug() << buffer.size();
        if (processData(buffer, m_pClient) != true)
        {
            QMessageBox::warning((QWidget *)this->parent(), tr("Error!"), tr("Got some garbage!"));
        }
    }
    qDebug() << "Exit\n";
} 

void server::incomingConnection(int descriptor) {
    if( !m_ServerSocket.setSocketDescriptor( descriptor ) ) {
        QMessageBox::warning( (QWidget *)this->parent(), tr("Error!"), tr("Socket error!") );
        return;
    }
    QMessageBox::information( (QWidget *)this->parent(), tr("Information"),tr("Got Data"));
}
