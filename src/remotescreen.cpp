#include <QSize>
#include <QDebug>
#include "remotescreen.h"

RemoteScreen::RemoteScreen()
    :QLabel(),
    m_owner(NULL),
    m_scale(1)
{
    m_pDisplay = new QImage(QSize(130,66), QImage::Format_Mono),
    m_pDisplay->fill(Qt::white);
}

void RemoteScreen::set_owner(QLabel *owner)
{
    m_owner = owner;
}

void RemoteScreen::draw_line(int num, int offset, const QByteArray &data)
{
    qDebug() << "Size: " << data.size() << "\n";
    for (int i = 0; i < data.size(); i++)
    {
        unsigned char c = data[i];
        for (int j = 0; j < 8; j++)
        {
            m_pDisplay->setPixel(offset + i + 1, (num * 8) + j + 1, c & 0x01 );
            c = c >> 1;
        }
    }
}

void RemoteScreen::update()
{
    qDebug() << "Running update\n";
    setPixmap(QPixmap::fromImage(m_pDisplay->scaled(8*130, 8*66)));
}

