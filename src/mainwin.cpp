#include <QApplication>
#include <QVBoxLayout>
#include <QSlider>
#include <QSize>
#include <QLabel>
#include "mainwin.h"
#include "ui_mainwin.h"
#include "network.h"
#include "remotescreen.h"

MainWin::MainWin(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    m_remote_layout = findChild<QVBoxLayout*>("remote_layout");
    m_remote_screen = new RemoteScreen();
    m_remote_layout->addWidget(m_remote_screen);
    m_server = new server(this, m_remote_screen);
    m_server->start_listen(10101);

}

MainWin::~MainWin()
{
    delete ui;
    delete m_remote_screen;
    delete m_server;
}

void MainWin::on_exit_clicked()
{
    QApplication::quit();
}

