#include <QApplication>
#include "mainwin.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    MainWin m;
    m.show();
    return app.exec();
}
