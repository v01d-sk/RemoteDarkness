#include <QtNetwork>
#include "remotescreen.h"

class QSignalMapper;

class server : public QTcpServer {
    Q_OBJECT
public:
    explicit server(QObject *parent, RemoteScreen *pSscreen);
    ~server();
public slots:
    void tcpReady();
    void tcpError( QAbstractSocket::SocketError error );
    bool start_listen(quint16 port_no);
    void initConnect();
    void clientReadyRead(QObject *socket);
protected:
    void incomingConnection( int descriptor );
    bool processData (const QByteArray &array, QTcpSocket *pClientSocket);
    bool send_ok(QTcpSocket *pClientSocket);
    RemoteScreen* m_pScreen;
	QTcpSocket* m_pConnection;
    QTcpSocket* m_pClient;
    QSignalMapper *m_pSignalMapper;
    QTcpSocket m_ServerSocket;
};


