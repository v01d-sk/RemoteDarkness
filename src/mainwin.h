#ifndef MAINWIN_H
#define MAINWIN_H

#include <QMainWindow>
#include <QLabel>
#include "remotescreen.h"

class QVBoxLayout;
class QSlider;

namespace Ui {
    class MainWindow;
}
class server;

class MainWin : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWin(QWidget *parent = 0);
    ~MainWin();

private slots:
    void on_exit_clicked();
private:
    Ui::MainWindow *ui;
    server *m_server;
    RemoteScreen *m_remote_screen;
    QVBoxLayout *m_remote_layout;
};

#endif // MAINWIN_H
