#ifndef _REMOTE_SCREEN_
#define _REMOTE_SCREEN_

#include <QLabel>
class RemoteScreen : public QLabel
{
    public:
        RemoteScreen();
        QImage get_image();
        void set_owner(QLabel *owner);
        void draw_line(int num, int offset, const QByteArray &data);
        void update();
    private:
        QImage *m_pDisplay;
        QImage *m_pImage;
        QLabel *m_owner;
        unsigned int m_scale;
};

#endif
